#  SRS WebRTC  

> 这篇文档将介绍, `SRS WebRTC` 的播放流程.  

目录:  

- [SRS Server 端](#srs-server-%E7%AB%AF)
- [客户端](#%E5%AE%A2%E6%88%B7%E7%AB%AF)

## SRS Server 端 


在 `SRS` 的服务器端, 我们会增加 `RTC` 的配置:  

```conf 
rtc_server {
    enabled on;
    # Listen at udp://8000
    listen 8083;
    #
    # The $CANDIDATE means fetch from env, if not configed, use * as default.
    #
    # The * means retrieving server IP automatically, from all network interfaces,
    # @see https://github.com/ossrs/srs/wiki/v4_CN_RTCWiki#config-candidate
    candidate $RTC_CANDIDATE;
}
```

从上面的注释能够看出, 我们在服务器端:  

- 监听了 `8083/udp` 端口  
- 读取环境变量 `RTC_CANDIDATE` (注意, 根据 `SDP` 这个不能用域名, 必须是 `IP`), 作为 `candidate`   


## 客户端  

`WebRTC` 的 `singaling` 过程在 `SRS` 里面很简单, 使用的是 `SRS 开源社区` 定义的 [RTCDN](https://github.com/rtcdn/rtcdn-draft) 协议:    


1. 客户端这边从浏览器获取 `SDP`   
2. 客户端通过 `POST` 请求, 请求 `SRS` 的 `/rtc/v1/play/` 接口, 如果请求成功, 将会收到 `SRS` 服务器端的 `SDP`.  


![](.imgs/2021-11-17-10-40-43.png)  


我们来看下具体的请求报文例子:  

```json
{
    "api": "https://qsc2.chinamcloud.cn:443/rtc/v1/play/",
    "tid": "64cf404",
    "streamurl": "webrtc://qsc2.chinamcloud.cn/live/preview1001_out",
    "clientip": null,
    "sdp": "v=0\r\no=mozilla...THIS_IS_SDPARTA-90.0.2 1417285783784529992 0 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=fingerprint:sha-256 93:FD:7A:AC:ED:61:F2:73:FE:74:A0:7E:04:C8:5F:A6:05:A3:B0:BC:16:06:3C:EF:24:AA:81:B5:01:F9:23:33\r\na=group:BUNDLE 0 1\r\na=ice-options:trickle\r\na=msid-semantic:WMS *\r\nm=audio 9 UDP/TLS/RTP/SAVPF 109 9 0 8 101\r\nc=IN IP4 0.0.0.0\r\na=recvonly\r\na=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\na=extmap:2/recvonly urn:ietf:params:rtp-hdrext:csrc-audio-level\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=fmtp:109 maxplaybackrate=48000;stereo=1;useinbandfec=1\r\na=fmtp:101 0-15\r\na=ice-pwd:a15b640b19b63b4ba53597fb8d96db7c\r\na=ice-ufrag:947959fc\r\na=mid:0\r\na=rtcp-mux\r\na=rtpmap:109 opus/48000/2\r\na=rtpmap:9 G722/8000/1\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:101 telephone-event/8000/1\r\na=setup:actpass\r\na=ssrc:2488352697 cname:{c5935de9-de2a-46a8-b410-03c897f3261d}\r\nm=video 9 UDP/TLS/RTP/SAVPF 120 124 121 125\r\nc=IN IP4 0.0.0.0\r\na=recvonly\r\na=extmap:3 urn:ietf:params:rtp-hdrext:sdes:mid\r\na=extmap:4 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\na=extmap:5 urn:ietf:params:rtp-hdrext:toffset\r\na=extmap:6/recvonly http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\na=extmap:7 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=fmtp:120 max-fs=12288;max-fr=60\r\na=fmtp:124 apt=120\r\na=fmtp:121 max-fs=12288;max-fr=60\r\na=fmtp:125 apt=121\r\na=ice-pwd:a15b640b19b63b4ba53597fb8d96db7c\r\na=ice-ufrag:947959fc\r\na=mid:1\r\na=rtcp-fb:120 nack\r\na=rtcp-fb:120 nack pli\r\na=rtcp-fb:120 ccm fir\r\na=rtcp-fb:120 goog-remb\r\na=rtcp-fb:120 transport-cc\r\na=rtcp-fb:121 nack\r\na=rtcp-fb:121 nack pli\r\na=rtcp-fb:121 ccm fir\r\na=rtcp-fb:121 goog-remb\r\na=rtcp-fb:121 transport-cc\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:120 VP8/90000\r\na=rtpmap:124 rtx/90000\r\na=rtpmap:121 VP9/90000\r\na=rtpmap:125 rtx/90000\r\na=setup:actpass\r\na=ssrc:3390240191 cname:{c5935de9-de2a-46a8-b410-03c897f3261d}\r\n"
}

```

注意到上面的 `sdp` 里面是客户端这边创建的 `SDP`.  

然后我们再看下返回的报文:  

```json

{
    "code": 0,
    "server": "vid-4u3z7a6",
    "sdp": "v=0\r\no=SRS/4.0.146(Leo) 36213728 2 IN IP4 0.0.0.0\r\ns=SRSPlaySession\r\nt=0 0\r\na=ice-lite\r\na=group:BUNDLE 0 1\r\na=msid-semantic: WMS live/preview1001_out\r\nm=audio 9 UDP/TLS/RTP/SAVPF 109\r\nc=IN IP4 0.0.0.0\r\na=ice-ufrag:f72z8088\r\na=ice-pwd:61984ls8z0y75h8291x6s112h34483yk\r\na=fingerprint:sha-256 4B:F4:DF:07:91:2F:0C:61:DD:BF:EA:5B:22:5E:74:9C:EB:82:CB:A7:29:D4:88:05:0D:79:97:7B:F4:9E:74:9E\r\na=setup:passive\r\na=mid:0\r\na=sendonly\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:109 opus/48000/2\r\na=ssrc:131810 cname:h2xuh7854628d594\r\na=ssrc:131810 label:audio-358b2pt6\r\na=candidate:0 1 udp 2130706431 188.131.164.124 8083 typ host generation 0\r\nm=video 9 UDP/TLS/RTP/SAVPF 126\r\nc=IN IP4 0.0.0.0\r\na=ice-ufrag:f72z8088\r\na=ice-pwd:61984ls8z0y75h8291x6s112h34483yk\r\na=fingerprint:sha-256 4B:F4:DF:07:91:2F:0C:61:DD:BF:EA:5B:22:5E:74:9C:EB:82:CB:A7:29:D4:88:05:0D:79:97:7B:F4:9E:74:9E\r\na=setup:passive\r\na=mid:1\r\na=extmap:7 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\na=sendonly\r\na=rtcp-mux\r\na=rtcp-rsize\r\na=rtpmap:126 H264/90000\r\na=rtcp-fb:126 nack\r\na=rtcp-fb:126 nack pli\r\na=rtcp-fb:126 transport-cc\r\na=fmtp:126 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f\r\na=ssrc:131811 cname:h2xuh7854628d594\r\na=ssrc:131811 label:video-8g283f74\r\na=candidate:0 1 udp 2130706431 188.131.164.124 8083 typ host generation 0\r\n",
    "sessionid": "f72z8088:dbbffd56"
}

```

同样, 在 `SDP` 字段里面, 包含了 `SRS Server` 的 `SDP`.  

我们来详细看下这里的 `SDP`:  

```
v=0
o=SRS/4.0.146(Leo) 36213728 2 IN IP4 0.0.0.0
s=SRSPlaySession
t=0 0
a=ice-lite
a=group:BUNDLE 0 1
a=msid-semantic: WMS live/preview1001_out
m=audio 9 UDP/TLS/RTP/SAVPF 109
c=IN IP4 0.0.0.0
a=ice-ufrag:f72z8088
a=ice-pwd:61984ls8z0y75h8291x6s112h34483yk
a=fingerprint:sha-256 4B:F4:DF:07:91:2F:0C:61:DD:BF:EA:5B:22:5E:74:9C:EB:82:CB:A7:29:D4:88:05:0D:79:97:7B:F4:9E:74:9E
a=setup:passive
a=mid:0
a=sendonly
a=rtcp-mux
a=rtcp-rsize
a=rtpmap:109 opus/48000/2
a=ssrc:131810 cname:h2xuh7854628d594
a=ssrc:131810 label:audio-358b2pt6
a=candidate:0 1 udp 2130706431 188.131.164.124 8083 typ host generation 0
m=video 9 UDP/TLS/RTP/SAVPF 126
c=IN IP4 0.0.0.0
a=ice-ufrag:f72z8088
a=ice-pwd:61984ls8z0y75h8291x6s112h34483yk
a=fingerprint:sha-256 4B:F4:DF:07:91:2F:0C:61:DD:BF:EA:5B:22:5E:74:9C:EB:82:CB:A7:29:D4:88:05:0D:79:97:7B:F4:9E:74:9E
a=setup:passive
a=mid:1
a=extmap:7 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01
a=sendonly
a=rtcp-mux
a=rtcp-rsize
a=rtpmap:126 H264/90000
a=rtcp-fb:126 nack
a=rtcp-fb:126 nack pli
a=rtcp-fb:126 transport-cc
a=fmtp:126 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f
a=ssrc:131811 cname:h2xuh7854628d594
a=ssrc:131811 label:video-8g283f74
a=candidate:0 1 udp 2130706431 188.131.164.124 8083 typ host generation 0

```

注意到这样一行:  

```
a=candidate:0 1 udp 2130706431 188.131.164.124 8083 typ host generation 0  
```

这里的 `188.131.164.124` 就是我们服务器端的 `candidate`, 而 `8083` 就是我们在服务器端配置的监听的 `UDP` 端口.  


当拿到了 `SDP` 之后, 剩下的过程就是 `WebRTC` 标准协议的部分了: `ICE`, 以及后面发送 `SRTP` 流, 这些都是浏览器需要操心的了.  





