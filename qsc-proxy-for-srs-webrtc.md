# QSC Proxy & QSC WebRTC Gateway 

- [Overview](#overview)
- [QSCProxy](#qscproxy)
- [QSC WebRTC Gateway](#qsc-webrtc-gateway)

## Overview  

我们先来回顾一下, 在没有网关的情况下, 我们在页面上播放 `QSC` 的 `WebRTC` 的流的流程:  

![](.imgs/2021-11-17-10-40-43.png)

- 首先是 `signaling` 的部分, `WebRTC` 实现的是 [rtcdn](https://github.com/rtcdn/rtcdn-draft), 我们使用浏览器收集到本地的 `SDP` 之后, 通过 http 请求, 请求 `QSC` 里面的 `SRS` 服务器的 `/rtc/v1/play` 接口, 在请求的响应中, 会拿到 `QSC` 服务器的 `SDP`.  

- 拿到了 `SDP` 之后, 由浏览器负责 `ICE`, 最终确定传输使用的 `candidate pair`, 连接建立成功之后, `QSC` 服务器通过 `UDP` 向客户端发送 `SRTP` 数据.  

而在有网关的情况下, 故事变成了下面这样的:  

![](.imgs/2021-11-17-10-52-40.png)   


- 首先是 `signaling` 的部分, 通过 `QSC Proxy` 做了反向代理到具体的 `QSC`, 这里不是简单的反向代理, 我们会修改 `SRS` 返回的 `sdp`, 修改 `端口` 和 `candidate`, 然后再发给客户端.   

- 然后是 `ICE` 和 后面的 `SRTP` 的数据, 是通过 `QSC WebRTC Gateway` 走的 `UDP` 的反向代理  
  - 注意到, 这里我们画了两个 `QSC` , 两个 `QSC` 都是监听的 `8083 UDP` 的端口, 但是要通过 `QSC WebRTC Gateway` 访问它们的时候, 我们走的是不同的端口(一个是 `40001`, 另一个是 `40002`), 最理想的状态是, 我们可以只监听一个端口, 但这需要我们的网关能够解析很多包: `STUN`, `RTP`, `RTCP`, 根据解析的结果做对应的转发, 但这工作量有点大, 我们暂时通过占用不同的端口来简化这个过程. 之后, 我们考虑做一个真正的 `WebRTC` 网关, 但那样的话, `signaling` 应该也需要合并.  


## QSCProxy 


在播放的时候, 我们请求地址形式变成:  

```
webrtc://qsc-proxy/live/preview1001_out?qsc=a15b640b19b63b4ba53597fb8d96db7c
```

注意到, 我们多加了一个 `query` 参数 `qsc`, 这个参数是对应的 `qsc` 的 `id`, 支持 `RTCDN` 的播放器, 在向服务器端发送 `POST` 请求的时候, 也会带上这个 `query` 参数, 我们的 `QSCProxy` 通过这个 `id`, 就知道应该把请求转发到具体哪个 `QSC`.    


![](.imgs/2021-11-17-11-15-59.png)   


当 `QSCProxy` 收到上游的 `QSC` 里面的 `SRS` 的响应之后, 假如所有的 `UDP` 流量都要走 `QSC WebRTC Gateway`, 我们要对其中的 `SDP` 做一些修改, 再返回给客户端, 我们其实只需要修改端口即可, 因为在部署的时候, 我们可以把 `QSC` 的 `candidate` 全部配置成 `QSC WebRTC Gateway` 的 `ip` 地址.   

比如, 下面这个例子里面, 如果请求的是 `webrtc://qsc-proxy/live/stream?qsc=abc`, 返回给客户端的 `candidate` 对应的 `端口` 会从 `8083` 被替换成 `40001`.   

![](.imgs/2021-11-17-11-23-34.png)  


那么, 我们剩下最后一个问题,  `qsc` 的 `id` 和它在 `QSC WebRTC Gateway` 上对应的 `udp` 端口, 这个映射是保存在哪里的?  

首先, `QSC` 部署, 我们会做一些限制:  

- 运行起来的 `QSC Pod` 都会被打上标签:  `qsc-cluster-node-type: qsc-instance`   
- `QSC Proxy` 会从 `k8s` 获取指定的 `namespace` 下面, 所有包含标签 `qsc-cluster-node-type: qsc-instance` 的 `pod`, 而 `Pod` 的 `ip + salt` 进行简单的 `hash` (sha256/md5sum等等) 就是这个 `qsc` 的 `id` .  
- `QSC Proxy` 将会创建和维护一个 `configmap`, 这个 `configmap` 里面的 `key` 是 `QSC Pod` 的 `IP`, 而 `Value` 就是分配到的 `UDP` 端口.   


> 从 `k8s` 监听 `Pod List` 可以参考 [SRSCluster Ingress Controller](https://gitlab.chinamcloud.com/StreamAtlas/SRSCluster-Ingress-Controller/blob/master/main.go). 


## QSC WebRTC Gateway   


这应该是一个类似 [SRSCluster Ingress Controller](https://gitlab.chinamcloud.com/StreamAtlas/SRSCluster-Ingress-Controller/blob/master/main.go) 的程序, 不同的地方是:  


- 在 `QSC WebRTC Gateway` 里面将会有一个 `nginx`, 这个 `nginx` 是作为一个 [UDP 的反向代理来使用](https://docs.nginx.com/nginx/admin-guide/load-balancer/tcp-udp-load-balancer/) (监听多个 `UDP` 端口, 为每个 QSC 监听一个).    

- `QSC WebRTC Gateway` 可以不用去监听 `Pod` 的变化, 它只需要监听 `Configmap` 的变化即可, 根据 `ConfigMap` 更新 `nginx` 的配置文件, 并且 `reload nginx`.  




