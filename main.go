package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
	"unsafe"

	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"

	"github.com/rs/cors"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
)

type InfoItem struct {
	Key  string
	IP   string
	Name string
}

type InfoPort struct {
	Port string `json:"ip"`
	IP   string `json:"port"`
}

type reqBody struct {
	Api       string `json:"api"`
	Tid       string `json:"tid"`
	Streamurl string `json:"Streamurl"`
	Clientip  string `json:"clientip"`
	Sdp       string `json:"sdp"`
}

type resBody struct {
	Code      int    `json:"code"`
	Sdp       string `json:"sdp"`
	Server    string `json:"server"`
	Sessionid string `json:"sessionid"`
	Simulator string `json:"simulator"`
}

var srsPods []*InfoItem

func handler(w http.ResponseWriter, r *http.Request) {
	// 把request的内容读取出来
	var bodyBytes []byte
	if r.Body != nil && r.Body != http.NoBody {
		bodyBytes, _ = ioutil.ReadAll(r.Body)
	} else {
		err := "failed to load http Body"
		log.Printf(err)
		SetResponseHeader(w)
		_, _ = w.Write(str2bytes(err))
		return
	}
	body := reqBody{}
	jsonErr := json.Unmarshal(bodyBytes, &body)
	if jsonErr != nil {
		err := "failed to unmarshal body!"
		log.Printf(err)
		SetResponseHeader(w)
		_, _ = w.Write(str2bytes(err))
		return
	}

	qscId := getQSCIdFromeHttp(body.Streamurl)
	qscIP := getIPSRS(srsPods, qscId)

	//发送给srs
	//xxj test
	//qscIP = "192.168.228.129:443"
	log.Printf("start request qsc")
	resBody := requestQSC("POST", "https://"+qscIP+"/rtc/v1/play/", bodyBytes)

	SetResponseHeader(w)
	w.WriteHeader(http.StatusOK)
	udpPort := getUdpPortBySrsIP(qscIP)
	resBody = setHttpResponse(resBody, udpPort)
	_, _ = w.Write(resBody)
}

func initPodIPConfig(srsPods *[]*InfoItem) {
	namespace, selectorKey, srsPodMatchValue, salt, err := LoadCfg()
	if err != nil {
		time.Sleep(time.Second * 1)
		panic(fmt.Errorf("failed to load config: %v ", err))
	}

	log.Printf("start watching pods in '%v' with selector of '%v=%v'",
		namespace, selectorKey, srsPodMatchValue)

	// creates the connection
	config, err := clientcmd.BuildConfigFromFlags("", os.Getenv("LOCAL_KUBE_CONFIG_FILE"))
	if err != nil {
		log.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	podListWatcher := cache.NewListWatchFromClient(clientset.CoreV1().RESTClient(), "pods", namespace, fields.Everything())

	indexer, informer := cache.NewIndexerInformer(podListWatcher, &v1.Pod{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			// 这里的 Add 是指对本地 cache 的 Add
			// 因此, 程序才启动的时候, 你能在这里拿到所有的 Pods
			key, _ := cache.MetaNamespaceKeyFunc(obj)
			log.Printf("a new pod added to cache: %v", key)
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, _ := cache.MetaNamespaceKeyFunc(new)
			log.Printf("a pod in cache was updated: %v", key)
		},
		DeleteFunc: func(obj interface{}) {
			key, _ := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			log.Printf("a pod in cache was removed: %v", key)
		},
	}, cache.Indexers{})

	stop := make(chan struct{})
	defer close(stop)
	go informer.Run(stop)

	// 等待 内部的 cache 同步完成
	if !cache.WaitForCacheSync(stop, informer.HasSynced) {
		log.Printf("WaitForCacheSync failed!")
		return
	}

	lastRouterConfig := make([]InfoPort, 0, 10) // 最近一次的 Nginx 的配置文件

	// 每 10s 钟时间, list 一次 cache 中的所有的 Pods
	// 尝试重新构造配置文件
	for range time.NewTicker(time.Second * 30).C {
		*srsPods = (*srsPods)[0:0]
		for _, v := range indexer.List() {

			pod, ok := v.(*v1.Pod)
			if !ok {
				continue
			}

			if pod.Status.Phase != v1.PodRunning {
				log.Printf("pod %v/%v is skipped, as it is not running (%v).", pod.Namespace, pod.Name, pod.Status.Reason)
				continue
			}
			//暂时 qsc命名空间下全是 stspod
			// if pod.Labels[selectorKey] == srsPodMatchValue { // 是否是要筛选的 Pod 类型
			*srsPods = append(*srsPods, &InfoItem{
				Key:  salt,
				Name: pod.Status.ContainerStatuses[0].Name,
				IP:   pod.Status.PodIP,
			})
			// } else {
			// log.Printf("pod %v/%v is skipped, as it does not match the selectors.", pod.Namespace, pod.Name)
			// }
		}

		conf, err := genRouterCfg(*srsPods)
		if err != nil {
			log.Printf("failed to execute template: %v", err)
			continue
		}

		if reflect.DeepEqual(conf, lastRouterConfig) {
			log.Printf("generated router config does not change")

		} else {
			//更新configmap
			log.Printf("generated nginx config changed ")
			mp := make(map[string]string)
			for _, info := range conf {
				mp[info.IP] = info.Port
			}
			configmap := &v1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "routercfg",
					Namespace: namespace,
				},
				Data: mp,
			}

			configMaps, err := clientset.CoreV1().ConfigMaps(namespace).List(context.TODO(), metav1.ListOptions{})
			if err != nil {
				log.Fatal(err)
			}

			configMapExists := false
			for _, cm := range configMaps.Items {
				if cm.GetName() == "routercfg" {
					configMapExists = true
				}
			}

			if configMapExists {
				_, err = clientset.CoreV1().ConfigMaps(namespace).Update(context.TODO(), configmap, metav1.UpdateOptions{})
			} else {
				_, err = clientset.CoreV1().ConfigMaps(namespace).Create(context.TODO(), configmap, metav1.CreateOptions{})
			}
			if err != nil {
				log.Printf("failed to reload router with new config: %v", err)
			} else {
				log.Printf("router config reloaded")
				lastRouterConfig = conf
			}
		}
	}
}

func main() {

	go initPodIPConfig(&srsPods)

	h := cors.New(
		cors.Options{
			AllowedOrigins: []string{"*"},
			AllowedHeaders: []string{"*"},
			AllowedMethods: []string{"*"},
			MaxAge:         300,
		}).Handler(http.HandlerFunc(handler))

	http.Handle("/", h)
	//xxj test
	//http.HandleFunc("/", handler)
	err := http.ListenAndServe(":8000", h)

	if err != nil {
		log.Printf("%v\n", "端口监听失败")
	}

}

func SetResponseHeader(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application-json")
	w.Header().Set("Connection", "close")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "x-requested-width,content-type")
}

func setHttpResponse(bodyBytes []byte, udpPort string) (resBytes []byte) {

	body := resBody{}
	jsonErr := json.Unmarshal(bodyBytes, &body)
	if jsonErr != nil {
		log.Printf("failed to Unmarshal json: %v", jsonErr)
		return
	}

	newSdp := setSdp(body.Sdp, udpPort)
	body.Sdp = newSdp

	//Marshal
	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		log.Printf("failed to Marshal json: %v", err)
		return
	}

	return bodyBytes
}

func genRouterCfg(srsPods []*InfoItem) ([]InfoPort, error) {
	startPort, _ := strconv.Atoi(os.Getenv("PORT_START"))
	endPort, _ := strconv.Atoi(os.Getenv("PORT_END"))
	var ports []InfoPort
	for i, pod := range srsPods {
		if startPort+i <= endPort {

			ports = append(ports, InfoPort{
				Port: strconv.Itoa(i + startPort),
				IP:   pod.IP,
			})
		}
	}

	return ports, nil
}

func getUdpPortBySrsIP(ip string) (port string) {
	ports, err := genRouterCfg(srsPods)
	if err != nil {
		log.Printf("failed to get port map: %v", err)
	}
	for _, item := range ports {
		if item.IP == ip {
			return item.Port
		}
	}

	log.Printf("no vailid port")
	return
}

func getQSCIdFromeHttp(streamUrl string) (qscHashId string) {

	if theIndex := strings.Index(streamUrl, "?qsc="); theIndex != -1 {
		slices := strings.Split(streamUrl, "?qsc=")
		return slices[1]
	}
	log.Printf("failed to get QSC ID!")
	return ""
}

func setSdp(sdp string, udpPort string) (res string) {

	if theIndex := strings.Index(sdp, "\r\n"); theIndex != -1 {
		slices := strings.Split(sdp, "\r\n")
		for i, v := range slices {
			if index := strings.Index(v, "candidate"); index != -1 {
				subSlices := strings.Fields(v) //根据空白符分割
				log.Printf("port = %v", subSlices[5])
				subSlices[5] = udpPort //修改端口
				subSlices[4] = "100.64.24.46"

				v = strings.Join(subSlices, " ") //重新组装
				slices[i] = v                    //修改端口
			} else {
				continue
			}
		}
		res = strings.Join(slices, "\r\n")
		return res
	}

	log.Printf("failed to setSdp port!")
	return ""
}

func LoadCfg() (ns string, key string, valueSRS string, salt string, err error) {

	ns = os.Getenv("NAMESPACE")
	if ns == "" {
		err = errors.New("missing 'NAMESPACE' in environment variable")
		return
	}

	key = os.Getenv("POD_SELECTOR_KEY")
	if key == "" {
		err = errors.New("missing 'POD_SELECTOR_KEY' in environment variable")
	}

	valueSRS = os.Getenv("POD_SRS_SELECTOR_VALUE")
	if valueSRS == "" {
		err = errors.New("missing 'POD_SRS_SELECTOR_VALUE' in environment variable")
	}

	salt = os.Getenv("SRS_SALT")

	if salt == "" {
		err = errors.New("missing 'SALT' in environment variable")
	}

	return
}

func getIPSRS(srsPods []*InfoItem, hashId string) (podIP string) {
	for index := range srsPods {
		s := []byte(srsPods[index].Name)
		key := []byte(srsPods[index].Key)
		m := hmac.New(sha256.New, key)
		m.Write(s)
		signature := hex.EncodeToString(m.Sum(nil))
		log.Printf("hashid=%v", signature)

		if hashId == signature {
			return srsPods[index].IP
		} else {
			continue
		}
	}

	log.Printf("failed to SRS Ip")
	return ""
}

func requestQSC(method string, url string, msg []byte) (res []byte) {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client := &http.Client{}
	body := bytes.NewBuffer([]byte(msg))

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		log.Printf("failed to new requeset to SRS: %v ", err)
		return
	}

	req.Header.Set("Content-Type", "application/json;charset=utf-8")

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("failed to requeset SRS: %v ", err)
		return
	}

	defer resp.Body.Close()

	res_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed to read Body: %v ", err)
		return
	}
	return res_body
}

func str2bytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

func bytes2str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
