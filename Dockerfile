FROM registry.cn-beijing.aliyuncs.com/cmc/qsc:golang-1.16 as builder

ADD .  /go/src/gitlab.chinamcloud.com/qsc/QSCProxy

RUN cd /go/src/gitlab.chinamcloud.com/qsc/QSCProxy && \
    GOFLAGS=-mod=vendor  go build -v .
 
ADD entry.shell /bin/entry.bash 
# ADD config  /bin/config

ENV NAMESPACE  qsc
ENV POD_SELECTOR_KEY  srs-cluster-node-type
ENV POD_SRS_SELECTOR_VALUE  qsc-instance
ENV SRS_SALT qsc-salt
ENV PORT_START 40001
ENV PORT_END  40010
#需要把k8s配置挂载到/bin
ENV LOCAL_KUBE_CONFIG_FILE  /bin/config

CMD ["bash","/bin/entry.bash"]
